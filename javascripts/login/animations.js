function slightTileExpand(firstTile, secondTile) {
   firstTile.css("width", "48%");
   secondTile.css("width", "42%");
}
function moveTile(firstTile, secondTile, delay) {
   if (secondTile.hasClass("expanded")) {
      secondTile.removeClass("expanded");
   }else if ( (firstTile.hasClass("expanded")) ) {
      return;
   }

   if( !(firstTile.hasClass("expanded")) ){
      firstTile.addClass("expanded");
   }

   rebaseForm(secondTile.find(".connection-tile-form"));

   delay = (delay === undefined)?0:delay;

   let frontContainerSlide = "down";
   let frontContainerFade = "out";

   slideAndFade(frontContainerSlide, frontContainerFade, secondTile.find(".front-container"));
   setTimeout(function () {
   firstTile.css("width", "75%");
   secondTile.css("width", "15%");

   }, delay);
}
function tileCollapse() {
   $("#signin").css("width", "45%");
   $("#signup").css("width", "45%");
}
function tileKeepExpandedCollapse(thisTile, secondTile) {
   thisTile.css("width", "15%");
   secondTile.css("width", "75%");
}
function slightCollapsedTileExpand(thisTile, secondTile){
   thisTile.css("width", "16%");
   secondTile.css("width", "74%");
}
function isExpanded() {
   if ($("#signin").hasClass("expanded") ||$("#signup").hasClass("expanded")) {
      return true;
   }else {
      return false;
   }
}
function highlight(el) {
   el.addClass("highlight");
}
function slideAndFade(slideMode, fadeMode, el, effectStrength, delay) {
   delay = (delay === undefined)?0:delay;
   effectStrength = (effectStrength === undefined)?"strong":effectStrength;

   let fade = (fadeMode === "out")?"fade-out":"fade-in";
   let slide = (slideMode === "up")?"slide-up-"+effectStrength:"slide-down-"+effectStrength;

   setTimeout(function () {
      el.addClass(fade);
   }, delay);
   el.addClass(slide);
}
function rebaseTiles() {
   let tiles = $("#signin, #signup");

   tiles.addClass("fast-transition");

   tiles.removeClass("expanded");
   tiles.find(".front-container").removeClass("slide-up-strong slide-down-strong fade-out");

   setTimeout(function () {
      tiles.removeClass("fast-transition");
   }, 400);
}
function positionMovingDot(position) {
   let dot = $("#moving-dot");
   let newDotPos = 0;
   let newPos = 0;

   switch (position) {
      case 1:
         newDotPos = getNextDotPos($(".dot:nth-child(2)"));
         newPos = getNewPos(newDotPos);
         dot.css("transform", "translateY("+newPos+"px)");
         break;
      case 2:
      // start from first dot not current dot
         newDotPos = getNextDotPos($(".dot:nth-child(3)"));
         newPos = getNewPos(newDotPos);
         dot.css("transform", "translateY("+newPos+"px)");
         break;
   }
}
function getNewPos(newDotPos) {
   let topDot = $(".dot:nth-child(1)");
   let topDotPos = topDot.offset().top;

   let result = newDotPos - topDotPos;

   return result;
}
function getNextDotPos(nextDot) {
   let result = nextDot.offset().top;
   return result;
}
function setMovingDot() {
   $("#moving-dot").css("transform", "translateY(0)");
}
function rebaseForm() {
   $(".connection-tile-form").addClass("fast-transition");
   $(".connection-tile-form").removeClass("fade-in slide-up-strong");

   setTimeout(function () {
      $(".connection-tile-form").removeClass("fast-transition dawn-flex");
   }, 400);
}
function rebaseSecondaryIcon() {
   $(".secondary-icon-container").removeClass("slide-down-light fade-in")
}
function rebaseTopLabel() {
   $(".tile-top-label").removeClass("fade-in slide-up-strong");
}
function screenWipe() {
   $("#wiper-screen").addClass("wipe");
   setTimeout(function () {
      $("#wiper-screen").removeClass("wipe");
   }, 2000);
}
function rebaseInputs() {
   $(":text, :password").val("");
   formInputAnimate();
}
function formInputAnimate(){
    var signinLogin = document.getElementById('signin-login');
    if (signinLogin.value != "") {
        $("#signin-login-bar").addClass("expand");
        $("#signin-login-label").addClass("signin-login-filled");
    }else {
        $("#signin-login-bar").removeClass("expand");
        $("#signin-login-label").removeClass("signin-login-filled");
    }

    var signinPassword = document.getElementById('signin-password');
    if (signinPassword.value != "") {
       $("#signin-password-bar").addClass("expand");
       $("#signin-password-label").addClass("signin-password-filled");
    }else {
       $("#signin-password-bar").removeClass("expand");
       $("#signin-password-label").removeClass("signin-password-filled");
    }

    var signupLogin = document.getElementById('signup-login');
    if (signupLogin.value != "") {
        $("#signup-login-bar").addClass("expand");
        $("#signup-login-label").addClass("signup-login-filled");
    }else {
        $("#signup-login-bar").removeClass("expand");
        $("#signup-login-label").removeClass("signup-login-filled");
    }

    var signupPassword = document.getElementById('signup-password');
    if (signupPassword.value != "") {
       $("#signup-password-bar").addClass("expand");
       $("#signup-password-label").addClass("signup-password-filled");
    }else {
       $("#signup-password-bar").removeClass("expand");
       $("#signup-password-label").removeClass("signup-password-filled");
    }

    var signupConfirmPassword = document.getElementById('signup-confirm-password');
    if (signupConfirmPassword.value != "") {
       $("#signup-confirm-password-bar").addClass("expand");
       $("#signup-confirm-password-label").addClass("signup-confirm-password-filled");
    }else {
       $("#signup-confirm-password-bar").removeClass("expand");
       $("#signup-confirm-password-label").removeClass("signup-confirm-password-filled");
    }

}
