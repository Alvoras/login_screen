$(document).ready(function () {
   $(".connection-tile-form").on("submit", function () {
      return;
   });

   formInputAnimate();

   let formInputs = $("#signin-login, #signin-password, #signup-login, #signup-password, #signup-confirm-password");

   formInputs.change(function () {
      formInputAnimate();
   });

   $("#signin, #signup").hover(function () {
      let thisTile = $(this);
      let secondTile = (thisTile.attr("id") === "signin")?$("#signup"):$("#signin");
      let notExpanded = ( !(thisTile.hasClass("expanded")) )?true:false;

      if ( notExpanded && isExpanded() ) {
         slightCollapsedTileExpand(thisTile, secondTile);
      }

      if ( (isExpanded()) ) {
         return;
      } else {
         slightTileExpand(thisTile, secondTile);
      }
   }, function () {
      let thisTile = $(this);
      let secondTile = (thisTile.attr("id") === "signin")?$("#signup"):$("#signin");
      let notExpanded = ( !(thisTile.hasClass("expanded")) )?true:false;

      if ( notExpanded && isExpanded() ) {
         tileKeepExpandedCollapse(thisTile, secondTile);
      }

      if ( (isExpanded()) ) {
         return;
      }else {
         tileCollapse();
      }
   });

   $("#signin, #signup").click(function () {
      let currentTile = $(this);

      if ( currentTile.hasClass("expanded") ) {
         return;
      }

      //currentTile.find(".connection-tile-form").removeClass("dawn-flex");

      rebaseForm();
      rebaseSecondaryIcon();
      rebaseTopLabel();

      let tileFadeMode = "out";
      let tileSlideMode = "up";
      slideAndFade(tileSlideMode, tileFadeMode, currentTile.find(".front-container"));

      let secondTile = (currentTile.attr("id") === "signin")?$("#signup"):$("#signin");

      let position = (currentTile.attr("id") === "signin")?1:2;

      positionMovingDot(position)

      moveTile(currentTile, secondTile, 400);

      let iconFadeMode = "in";
      let iconSlideMode = "down";

      let topLabelFadeMode = "in";
      let topLabelSlideMode = "down";

      if ( currentTile.hasClass !== "expanded" ) {
         slideAndFade(topLabelSlideMode, topLabelFadeMode, currentTile.find(".tile-top-label"), "strong", 400);
         slideAndFade(iconSlideMode, iconFadeMode, secondTile.find(".secondary-icon-container"), "light", 200);
      }

      setTimeout(function () {
         currentTile.find(".connection-tile-form").addClass("dawn-flex");
      }, 400);

      let formFadeMode = "in";
      let formSlideMode = "up";

      setTimeout(function () {
         slideAndFade(formSlideMode, formFadeMode, currentTile.find(".connection-tile-form"));
      }, 800);
   });
   $("#reset").click(function () {
      screenWipe();
      setTimeout(function () {
         tileCollapse();
         rebaseTopLabel();
         rebaseTiles();
         rebaseSecondaryIcon();
         rebaseForm();
         setMovingDot();
         rebaseInputs();
      }, 200);
   });
});
