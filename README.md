# Login Screen 1.0.0
   Inspired by <b>Nikita Fedorov</b> with https://dribbble.com/shots/2799169-WeeklyUI-001-Sign-In-and-Sign-Out, I thought I should give this a try. So I made my own version with some small changes ; it's made within a node.js environnement as I plan to reuse this for a project later.

![Landing page](http://i.imgur.com/kjYyaBn.jpg)

![Login page](http://i.imgur.com/vWSF12y.jpg)
